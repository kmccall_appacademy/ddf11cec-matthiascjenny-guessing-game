contents = File.readlines(ARGV[0])
contents.shuffle!
file_name_arr = ARGV[0].split(".")
file_name_arr.pop
file_name = file_name_arr.join(".")
out_file = File.new("#{file_name}-shuffled.txt", "w")
out_file.puts(contents)
out_file.close
